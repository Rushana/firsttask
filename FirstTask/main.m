//
//  main.m
//  FirstTask
//
//  Created by rushana on 26.09.14.
//  Copyright (c) 2014 rushana. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
