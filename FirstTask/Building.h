//
//  Building.h
//  FirstTask
//
//  Created by rushana on 26.09.14.
//  Copyright (c) 2014 rushana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Building : NSObject

typedef enum buildingMaterial {brick, panel, monolithic} buildingMaterial;

//  FIXME: if you put @property it public interface, you don't have to write prototypes in a header

- (void)setNumberOfFloors:(int)numberOfFloors;
- (int)  numberOfFloors;

- (void)setMaterial:(buildingMaterial)material;
- (buildingMaterial) material;

@end
