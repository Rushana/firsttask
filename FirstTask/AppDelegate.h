//
//  AppDelegate.h
//  FirstTask
//
//  Created by rushana on 26.09.14.
//  Copyright (c) 2014 rushana. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
