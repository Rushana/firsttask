//
//  AppDelegate.m
//  FirstTask
//
//  Created by rushana on 26.09.14.
//  Copyright (c) 2014 rushana. All rights reserved.
//

#import "AppDelegate.h"
#import "Building.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    Building* skysraper = [[Building alloc] init];
    [skysraper setNumberOfFloors:11];
    NSLog(@"numberOfFloors = %d", [skysraper numberOfFloors]);
    [skysraper setMaterial:brick];
    NSLog(@"material = %d", [skysraper material]);
}

@end
