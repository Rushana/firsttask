//
//  Building.m
//  FirstTask
//
//  Created by rushana on 26.09.14.
//  Copyright (c) 2014 rushana. All rights reserved.
//

#import "Building.h"

// limitations for numberOfFloors
const int MAXNUM_FOR_BRICK_BUILDING = 10;
const int MAXNUM_FOR_PANEL_BUILDING = 20;
const int MINNUM_OF_FLOORS = 1;
const int MAXNUM_OF_FLOORS = 200;

//  FIXME:  we use @property instead of these old style syntax
@implementation Building {
    int _numberOfFloors;
    enum buildingMaterial _material;
}

- (bool) isConstructionConsistentWithMaterial:(buildingMaterial)material WithNumberOfFloors:(int)numberOfFloors
{
    switch (material) {
        case brick:
            if (numberOfFloors > MAXNUM_FOR_BRICK_BUILDING) return NO;
            break;
        case panel:
            if (numberOfFloors > MAXNUM_FOR_PANEL_BUILDING) return NO;
            break;
        case monolithic:
            // you can build any number of floors (up to max)
            // with monolithic material (or you can put limitations later)
            break;
        default: break;
    }
    return YES;
}

- (void) setNumberOfFloors:(int)numberOfFloors
{
    if ((numberOfFloors <= MINNUM_OF_FLOORS) && (numberOfFloors > MAXNUM_OF_FLOORS)) return;
    if (![self isConstructionConsistentWithMaterial:_material WithNumberOfFloors:numberOfFloors])
          return;
    self->_numberOfFloors = numberOfFloors;
}

//  FIXME: if you use @property, you don't have to rewrite getter
- (int) numberOfFloors
{
    return self->_numberOfFloors;
}

- (void) setMaterial:(buildingMaterial)material
{
    if (![self isConstructionConsistentWithMaterial:material WithNumberOfFloors:_numberOfFloors])
        return;
    self->_material = material;
}

//  FIXME: if you use @property, you don't have to rewrite getter
- (buildingMaterial) material
{
    return self->_material;
}


@end